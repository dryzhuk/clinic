CENTER DERMATOLOGIE
--
- View site: http://dryzhuk.gitlab.io/clinic/
- Version 1.0
- Author: Roman Dryzhuk 


## Configuration:

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/dryzhuk/clinic/blob/master/.gitlab-ci.yml)

## More Info:
- https://t.me/dryzhuk